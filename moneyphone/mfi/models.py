from curses import ALL_MOUSE_EVENTS
from statistics import mode
from unicodedata import name
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator ,RegexValidator

# Create your models here.
class Account(models.Model):
    names=models.CharField(max_length=50,help_text="First & Last Name")
    email=models.EmailField(max_length=60,unique=True)
    phone_number=models.CharField(max_length=10,unique=True)

    def __str__(self):
        return self.names

class Business(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    RETAIL = 'RT'
    PROFESSIONAL = 'PR'
    FOOD_DRINKS = 'FD'
    ENTERTAINMENT = 'ET'
   
   #choice list
    SECTOR_CHOICES = [
        (RETAIL, 'Retail'),
        (PROFESSIONAL, 'Professional Services'),
        (FOOD_DRINKS, 'Food & Drinks'),
        (ENTERTAINMENT, 'Entertainment'),
    ]

    name=models.CharField(max_length=50)
    address=models.CharField(max_length=50)

    #regex to ensure enforce validation of number of digits.CharField used because integerfield maxlength is ignored by django
    reg_company=models.CharField(
        max_length=10, 
        help_text="8 max digits for company registration",
        validators=[RegexValidator(r'^\d{1,8}$')]
    )
    sector =models.CharField(
        max_length=2,
        choices=SECTOR_CHOICES,
        default=PROFESSIONAL,
    )

    def __str__(self):
        return self.name

class Loan(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE)

    #enforce amount validation 
    amount=models.PositiveIntegerField(
        help_text="Amount in $",
        validators=[
            MaxValueValidator(100000),
            MinValueValidator(10000)
        ]
    )
    repayment_period=models.PositiveIntegerField()
    loan_reason=models.TextField()