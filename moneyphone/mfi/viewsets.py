from rest_framework import viewsets 
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import *
from .models import *
from rest_framework.permissions import AllowAny,IsAdminUser

class AccountViewSet(viewsets.ModelViewSet):
    # http_method_names = ['post','get']
    # permission_classes = (AllowAny,)
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

class BusinessViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAdminUser,)
    # ermission_classes = (AllowAny,)
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer

class LoanViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAdminUser,)
    # ermission_classes = (AllowAny,)
    queryset = Loan.objects.all()
    serializer_class = LoanSerializer