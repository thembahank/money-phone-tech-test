from rest_framework import serializers
from .models import *
from django.contrib.auth import authenticate

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'names','email','phone_number')

class BusinessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Business
        fields = ('id', 'name','address','account','reg_company')

class LoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = ('id', 'business','amount','repayment_period','loan_reason')